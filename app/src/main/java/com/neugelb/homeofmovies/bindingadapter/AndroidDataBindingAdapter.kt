package com.neugelb.homeofmovies.feature.bindingadapter

import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.feature.application.HomeMovieApplication
import com.neugelb.homeofmovies.feature.model.Movie

object AndroidDataBindingAdapter {

    @JvmStatic
    @BindingAdapter(value = ["adapter", "gridLayoutManager"], requireAll = false)
    fun setRecycleViewAdapter(
        recyclerView: RecyclerView, adapter: RecyclerView.Adapter<*>?, gridLayoutManager: Boolean
    ) {
        if (gridLayoutManager) {
            recyclerView.layoutManager = GridLayoutManager(recyclerView.context, 2)
        } else {
            recyclerView.layoutManager = LinearLayoutManager(recyclerView.context).apply {
                this.reverseLayout = reverseLayout
            }
        }
        if (recyclerView.adapter == null) {
            recyclerView.adapter = adapter
        }
    }

    @JvmStatic
    @BindingAdapter("goneUnless")
    fun goneUnless(view: View, visible: Boolean) {
        view.visibility = if (visible) View.VISIBLE else View.GONE
    }

    @JvmStatic
    @BindingAdapter(value = ["bind:srcUrl", "usePlaceHolder"], requireAll = false)
    fun setImageResource(imageView: ImageView, srcUrl: String?, usePlaceHolder: Boolean) {
        srcUrl?.let {
            val requesetBuilder =
                Glide.with(HomeMovieApplication.instance.applicationContext).load(it)
                    .transforms(CenterCrop(), RoundedCorners(20))
                    .transition(withCrossFade())
            if (usePlaceHolder) {
                requesetBuilder.placeholder(R.drawable.fallback)
                    .fallback(R.drawable.fallback)
            }
            requesetBuilder.into(imageView)
        }
    }
}