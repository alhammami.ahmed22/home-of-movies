package com.neugelb.homeofmovies.feature.manager

import androidx.paging.DataSource
import com.neugelb.homeofmovies.data.getSimple
import com.neugelb.homeofmovies.feature.application.HomeMovieApplication
import com.neugelb.homeofmovies.feature.model.Movie
import com.neugelb.homeofmovies.feature.model.MovieList
import com.neugelb.homeofmovies.network.ApiInterface
import com.neugelb.homeofmovies.feature.network.ICallBack
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import rx.Observable
import rx.subjects.BehaviorSubject
import java.util.*
import javax.inject.Inject

class MovieModelManager {
    @Inject
    lateinit var apiInterface: ApiInterface

    var selectedMovieSubscription: BehaviorSubject<Movie> = BehaviorSubject.create()

    var selectedMovie: Movie? = null
        set(value) {
            field = value
            this.selectedMovieSubscription.onNext(value!!)
        }

    init {
        HomeMovieApplication.instance.managerComponent.inject(this)
    }

    fun fetchMovieDetails(movieId: String, apiKey: String, callBack: ICallBack<Movie>) {
        apiInterface.fetchMovieDetails(movieId, apiKey).enqueue(object : Callback<Movie> {
            override fun onFailure(call: Call<Movie>, t: Throwable) {
                callBack.onFailure()
            }

            override fun onResponse(call: Call<Movie>, response: Response<Movie>) {
                callBack.onSuccess(response.body())
            }
        });
    }

    fun getMovieSubscription(): Observable<Movie> {
        return this.selectedMovieSubscription.asObservable()
    }

}