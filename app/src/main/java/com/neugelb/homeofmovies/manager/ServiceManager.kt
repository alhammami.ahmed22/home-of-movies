package com.neugelb.homeofmovies.manager

import com.neugelb.homeofmovies.BuildConfig
import com.neugelb.homeofmovies.network.ApiInterface
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceManager {
    var apiBaseUrl = "https://api.themoviedb.org/3/"
    lateinit var retrofit: Retrofit
    private var apiInterface: ApiInterface? = null

    private var builder: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .baseUrl(apiBaseUrl)

    init {
        setUpOkHttpClient()
    }

    private fun setUpOkHttpClient() {
        val loggingInterceptor = HttpLoggingInterceptor()
        loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        val okHttpBuilder = OkHttpClient.Builder()

        if (BuildConfig.DEBUG) {
            okHttpBuilder.addInterceptor(loggingInterceptor)
        }

        retrofit = builder.client(okHttpBuilder.build()).build()
    }

    fun createService(): ApiInterface {
        if (apiInterface == null) {
            apiInterface = retrofit.create(ApiInterface::class.java)
        }
        return apiInterface!!
    }
}