package com.neugelb.homeofmovies.feature.network

interface ICallBack<E> {
    fun onSuccess(item: E?)
    fun onFailure()
}