package com.neugelb.homeofmovies.network

import com.neugelb.homeofmovies.feature.model.Movie
import com.neugelb.homeofmovies.feature.model.MovieList
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiInterface {
    @GET("discover/movie?certification_country=US&adult=false&with_original_language=en&sort_by=primary_release_date.desc")
    fun fetchPopularMovies(
        @Query("api_key") apiKey: String,
        @Query("primary_release_date.lte") releaseDate: String,
        @Query("page") page: Int
    ): Call<MovieList>

    @GET("movie/{movie_id}?append_to_response=videos")
    fun fetchMovieDetails(
        @Path("movie_id") movieId: String,
        @Query("api_key") apiKey: String
    ): Call<Movie>
}
