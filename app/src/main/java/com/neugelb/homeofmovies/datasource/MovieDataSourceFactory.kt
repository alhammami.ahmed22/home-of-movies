package com.neugelb.homeofmovies.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.neugelb.homeofmovies.feature.model.Movie

class MovieDataSourceFactory() : DataSource.Factory<Int, Movie>() {
    val movieDataSource = MovieDataSource()
    private val showsDataSourceLiveData: MutableLiveData<MovieDataSource> = MutableLiveData()

    override fun create(): DataSource<Int, Movie> {
        showsDataSourceLiveData.postValue(movieDataSource)
        return movieDataSource
    }
}