package com.neugelb.homeofmovies.datasource

import androidx.paging.PageKeyedDataSource
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.data.getSimple
import com.neugelb.homeofmovies.feature.application.HomeMovieApplication
import com.neugelb.homeofmovies.feature.model.Movie
import com.neugelb.homeofmovies.feature.model.MovieList
import com.neugelb.homeofmovies.network.ApiInterface
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import javax.inject.Inject


class MovieDataSource : PageKeyedDataSource<Int, Movie>() {
    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Movie>
    ) {
        apiInterface.fetchPopularMovies(
            HomeMovieApplication.instance.resources.getString(R.string.api_key),
            Date().getSimple(), 1
        ).enqueue(object : Callback<MovieList> {
            override fun onFailure(call: Call<MovieList>, t: Throwable) {
            }

            override fun onResponse(call: Call<MovieList>, response: Response<MovieList>) {
                if(response.isSuccessful){
                    callback.onResult(response.body()?.results ?: emptyList(), null, 2)
                }
            }
        })
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
        apiInterface.fetchPopularMovies(
            HomeMovieApplication.instance.resources.getString(R.string.api_key),
            Date().getSimple(), params.key
        )
            .enqueue(object : Callback<MovieList> {
                override fun onFailure(call: Call<MovieList>, t: Throwable) {
                }

                override fun onResponse(call: Call<MovieList>, response: Response<MovieList>) {
                    if(response.isSuccessful){
                        val body = response.body()
                        val nextKey = if (params.key == body?.total_pages) null else params.key + 1
                        callback.onResult(body?.results ?: emptyList(), nextKey)
                    }
                }
            })
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, Movie>) {
    }

    @Inject
    lateinit var apiInterface: ApiInterface

    init {
        HomeMovieApplication.instance.managerComponent.inject(this)
    }
}