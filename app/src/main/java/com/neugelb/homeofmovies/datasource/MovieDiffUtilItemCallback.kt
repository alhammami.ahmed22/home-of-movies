package com.neugelb.homeofmovies.datasource

import androidx.recyclerview.widget.DiffUtil
import com.neugelb.homeofmovies.feature.model.Movie

class MovieDiffUtilItemCallback : DiffUtil.ItemCallback<Movie>() {
    override fun areItemsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Movie, newItem: Movie): Boolean {
        return oldItem.title == newItem.title
    }
}