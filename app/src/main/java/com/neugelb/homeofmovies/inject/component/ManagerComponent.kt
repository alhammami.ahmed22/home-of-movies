package com.neugelb.homeofmovies.feature.inject.component

import com.neugelb.homeofmovies.datasource.MovieDataSource
import com.neugelb.homeofmovies.datasource.MovieDataSourceFactory
import com.neugelb.homeofmovies.feature.inject.module.ManagerModule
import com.neugelb.homeofmovies.feature.manager.MovieModelManager
import com.neugelb.homeofmovies.inject.scope.UserScope
import dagger.Component
import javax.inject.Singleton

@Singleton
@UserScope
@Component(modules = [ManagerModule::class])
interface ManagerComponent {
    fun inject(movieModelManager: MovieModelManager)
    fun inject(moviedDataSource: MovieDataSource)
}
