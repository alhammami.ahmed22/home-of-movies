package com.neugelb.homeofmovies.feature.inject.module

import com.google.gson.Gson
import com.neugelb.homeofmovies.inject.scope.UserScope
import com.neugelb.homeofmovies.manager.ServiceManager
import com.neugelb.homeofmovies.network.ApiInterface
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ManagerModule {

    @Provides
    @Singleton
    fun provideRestAPi(): ApiInterface {
        return ServiceManager().createService()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }
}