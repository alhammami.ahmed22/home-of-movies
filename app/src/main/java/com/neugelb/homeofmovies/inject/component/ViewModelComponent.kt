package com.neugelb.homeofmovies.feature.inject.component

import com.neugelb.homeofmovies.feature.inject.module.ViewModelModule
import com.neugelb.homeofmovies.feature.moviedetails.MovieDetailsActivityViewModel
import com.neugelb.homeofmovies.feature.movielist.MovieListActivityViewModel
import com.neugelb.homeofmovies.inject.scope.UserScope
import dagger.Component
import javax.inject.Singleton

@UserScope
@Singleton
@Component(modules = [ViewModelModule::class])
interface ViewModelComponent {
    fun inject(movieListActivityViewModel: MovieListActivityViewModel)
    fun inject(movieDetailsActivityViewModel: MovieDetailsActivityViewModel)
}
