package com.neugelb.homeofmovies.feature.inject.module

import com.neugelb.homeofmovies.datasource.MovieDataSourceFactory
import com.neugelb.homeofmovies.feature.manager.MovieModelManager
import com.neugelb.homeofmovies.inject.scope.UserScope
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelModule {

    @Provides
    @Singleton
    fun provideMovieModelManager(): MovieModelManager {
        return MovieModelManager()
    }

    @Provides
    @UserScope
    fun provideMovieDataSourceFactory(): MovieDataSourceFactory {
        return MovieDataSourceFactory()
    }
}