package com.neugelb.homeofmovies.data

import java.text.SimpleDateFormat
import java.util.*

fun Date.getSimple(): String {
    val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
    return dateFormat.format(this)
}

fun Date.getSimple(pattern: String): String {
    val dateFormat = SimpleDateFormat(pattern, Locale.ENGLISH)
    return dateFormat.format(this)
}

fun String.reformatDate(fromPattern: String, toPattern: String): String {
    val dateFrom = SimpleDateFormat(fromPattern, Locale.ENGLISH)
    val date = dateFrom.parse(this)
    return date.getSimple(toPattern)
}

fun Int.toReadableTime(): String {
    val hour = this / 60
    val rest = this % 60
    return "$hour h $rest m"
}