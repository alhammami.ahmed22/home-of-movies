package com.neugelb.homeofmovies.feature.moviedetails

import android.content.ActivityNotFoundException
import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.databinding.ActivityMoviedetailsBinding


class MovieDetailsActivity : AppCompatActivity(), MovieDetailsNavigator {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMoviedetailsBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_moviedetails)
        val viewModel = ViewModelProviders.of(this)[MovieDetailsActivityViewModel::class.java]
        viewModel.setUpView()
        viewModel.navigator = this
        binding.viewModel = viewModel

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayShowTitleEnabled(false);
        supportActionBar!!.setBackgroundDrawable(
            ColorDrawable(ContextCompat.getColor(this, android.R.color.transparent))
        )
    }

    override fun watchMovieTrailer(key: String) {
        val appIntent = Intent(Intent.ACTION_VIEW, Uri.parse("vnd.youtube:$key"))
        val webIntent = Intent(
            Intent.ACTION_VIEW,
            Uri.parse("http://www.youtube.com/watch?v=$key")
        )
        try {
            startActivity(appIntent)
        } catch (ex: ActivityNotFoundException) {
            startActivity(webIntent)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}

interface MovieDetailsNavigator {
    fun watchMovieTrailer(key: String)
}
