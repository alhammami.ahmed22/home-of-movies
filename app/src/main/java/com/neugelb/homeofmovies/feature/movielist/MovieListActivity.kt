package com.neugelb.homeofmovies.feature.movielist

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.databinding.ActivityMovielistBinding
import com.neugelb.homeofmovies.feature.moviedetails.MovieDetailsActivity


class MovieListActivity : AppCompatActivity(), MovieListNavigator {

    lateinit var viewModel: MovieListActivityViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMovielistBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_movielist)
        viewModel = ViewModelProviders.of(this)[MovieListActivityViewModel::class.java]
        viewModel.navigator = this
        binding.viewModel = viewModel

        viewModel.getMovieLiveData().observe(this, Observer {
            viewModel.submitList(it)
        })
    }

    override fun navigateToMovieDetailsView() {
        startActivity(Intent(this, MovieDetailsActivity::class.java))
    }
}

interface MovieListNavigator {
    fun navigateToMovieDetailsView()
}
