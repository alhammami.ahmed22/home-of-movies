package com.neugelb.homeofmovies.feature.moviedetails

import android.app.Application
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.AndroidViewModel
import com.neugelb.homeofmovies.BR
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.feature.application.HomeMovieApplication
import com.neugelb.homeofmovies.feature.manager.MovieModelManager
import com.neugelb.homeofmovies.feature.model.Movie
import com.neugelb.homeofmovies.feature.network.ICallBack
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers
import javax.inject.Inject

class MovieDetailsActivityViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var movieModelManager: MovieModelManager

    var bindingModel = MovieDetailsActivityBindingModel()
    lateinit var navigator: MovieDetailsNavigator

    init {
        HomeMovieApplication.instance.viewModelComponent.inject(this)
    }

    fun setUpView() {
        movieModelManager.selectedMovieSubscription
            .subscribeOn(Schedulers.newThread())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                fetchMovieDetails(it)
            }
    }

    private fun fetchMovieDetails(movie: Movie) {
        movieModelManager.fetchMovieDetails(
            movie.id,
            getApplication<HomeMovieApplication>().resources.getString(R.string.api_key),
            object : ICallBack<Movie> {
                override fun onSuccess(item: Movie?) {
                    bindingModel.movie = item
                }

                override fun onFailure() {
                }
            })
    }

    fun watchMovieTrailer() {
        val key = bindingModel.movie!!.videos!!.results[0].key
        navigator.watchMovieTrailer(key)

    }

    inner class MovieDetailsActivityBindingModel : BaseObservable() {
        @Bindable
        var movie: Movie? = null
            set(value) {
                field = value
                notifyPropertyChanged(BR.movie)
            }
    }
}

