package com.neugelb.homeofmovies.feature.movielist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.neugelb.homeofmovies.datasource.MovieDataSourceFactory
import com.neugelb.homeofmovies.datasource.MovieDiffUtilItemCallback
import com.neugelb.homeofmovies.feature.application.HomeMovieApplication
import com.neugelb.homeofmovies.feature.manager.MovieModelManager
import com.neugelb.homeofmovies.feature.model.Movie
import javax.inject.Inject

class MovieListActivityViewModel(application: Application) : AndroidViewModel(application) {

    @Inject
    lateinit var movieModelManager: MovieModelManager
    @Inject
    lateinit var movieDataSourceFactory: MovieDataSourceFactory

    private lateinit var movieLiveData: LiveData<PagedList<Movie>>
    var movieListAdapter = MovieListAdapter(this, MovieDiffUtilItemCallback())

    lateinit var navigator: MovieListNavigator

    init {
        HomeMovieApplication.instance.viewModelComponent.inject(this)
        setUp()
    }

    fun setUp() {
        val config = PagedList.Config.Builder()
            .setPageSize(20)
            .setEnablePlaceholders(false)
            .setInitialLoadSizeHint(20)
            .build()

        movieLiveData = LivePagedListBuilder(movieDataSourceFactory, config).build()
    }

    fun getMovieLiveData(): LiveData<PagedList<Movie>> {
        return movieLiveData
    }

    fun submitList(movieListList: PagedList<Movie>) {
        movieListAdapter.submitList(movieListList)
    }

    fun navigateToMovieDetails(movie: Movie) {
        movieModelManager.selectedMovie = movie
        navigator.navigateToMovieDetailsView()
    }
}