package com.neugelb.homeofmovies.feature.movielist

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.neugelb.homeofmovies.BR
import com.neugelb.homeofmovies.R
import com.neugelb.homeofmovies.feature.model.Movie

open class MovieListAdapter(
    var viewModel: MovieListActivityViewModel,
    diffCallback: DiffUtil.ItemCallback<Movie>
) : PagedListAdapter<Movie, MovieListAdapter.MovieViewHolder>(diffCallback) {

    private var layoutInflater: LayoutInflater? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        if (layoutInflater == null) {
            layoutInflater = LayoutInflater.from(parent.context)
        }
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            layoutInflater!!, R.layout.item_movie, parent, false
        )
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(getItem(position)!!)
    }

    inner class MovieViewHolder(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Movie) {
            binding.setVariable(BR.model, item)
            binding.setVariable(BR.viewModel, viewModel)
            binding.setVariable(BR.position, adapterPosition)
            binding.executePendingBindings()
        }
    }
}