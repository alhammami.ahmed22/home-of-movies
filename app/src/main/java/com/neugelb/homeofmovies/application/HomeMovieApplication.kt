package com.neugelb.homeofmovies.feature.application

import android.app.Application
import androidx.multidex.MultiDexApplication
import com.neugelb.homeofmovies.feature.inject.component.DaggerManagerComponent
import com.neugelb.homeofmovies.feature.inject.component.DaggerViewModelComponent
import com.neugelb.homeofmovies.feature.inject.component.ManagerComponent
import com.neugelb.homeofmovies.feature.inject.component.ViewModelComponent

class HomeMovieApplication :  MultiDexApplication() {

    lateinit var viewModelComponent: ViewModelComponent
    lateinit var managerComponent: ManagerComponent

    override fun onCreate() {
        super.onCreate()
        instance = this
        viewModelComponent = createViewModelComponent()
        managerComponent = createManagerComponent()
    }

    private fun createViewModelComponent(): ViewModelComponent {
        return DaggerViewModelComponent.builder().build()
    }

    private fun createManagerComponent(): ManagerComponent {
        return DaggerManagerComponent.builder().build()
    }

    companion object {
        lateinit var instance: HomeMovieApplication
    }
}