package com.neugelb.homeofmovies.feature.model

class MovieList {
    var pages: Int = 0
    var total_results: Int = 0
    var total_pages: Int = 0
    var results: ArrayList<Movie> = ArrayList()
}