package com.neugelb.homeofmovies.feature.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.neugelb.homeofmovies.data.reformatDate
import com.neugelb.homeofmovies.data.toReadableTime

class Movie {
    val THUMBNAIL_URL = "https://image.tmdb.org/t/p/original"
    val BACKDROP_URL = "https://image.tmdb.org/t/p/w300"

    var id: String = ""
    var adult: Boolean = false
    var title: String = ""
    var budget: Int = 0
    var homepage: String = ""
    var overview: String = ""
        get() {
            return if (field.isBlank()) "Content not availabe" else field
        }
    var popularity: Float = 0.toFloat()
    var revenue: Int = 0
    var runtime: Int = 0
    var status: String = ""
    var tagline: String = ""
    var video: Boolean = false
    var videos: Videos? = null


    @SerializedName("release_date")
    @Expose
    var releaseDate: String = ""
        get() {
            return field.reformatDate("yyyy-MM-dd", "yyyy")
        }

    @SerializedName("vote_average")
    @Expose
    var voteAverage: Float = 0.toFloat()

    @SerializedName("genres")
    @Expose
    var genres: ArrayList<Genre> = ArrayList()

    var hasVideo = false
        get() {
            return videos?.results?.isNotEmpty() ?: false
        }

    var poster_path: String = ""
        get() {
            return THUMBNAIL_URL + field
        }

    var simplifiedAverage = 0f
        get() {
            return (voteAverage / 10) * 5
        }

    var readableRunTime = ""
        get() {
            return if (runtime == 0) "" else runtime.toReadableTime()
        }

    var movieGenre = ""
        get() {
            if (genres.isEmpty()) return "Unclassified"
            else {
                var genre = ""
                for ((index, value) in genres.withIndex()) {
                    genre = genre.plus(value.name)
                    if (index > 0 || index < genres.size - 2) {
                        genre = genre.plus(" / ")
                    }
                }
                return genre
            }
        }

    inner class Genre {
        var id: Int = 0
        var name: String = ""
    }

    inner class Videos {
        var results: List<Result> = ArrayList()
    }

    inner class Result {
        var key: String = ""
        var name: String = ""
    }

}